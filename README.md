# CubeSat-Radio
Original KiCad project of the cubesat radio test article in preparation for ORCASat

This repository uses the SFU Satellite Team's KiCad component library as a submodule.
To start working on the repository:

```
1. Open the git shell
2. Navigate to the directory you wish to place the repository inside
3. git clone --recurse-submodules git@gitlab.com:ORCASat/COMS/CubeSat-Radio.git
4. cd CubeSat-Radio/KiCad-Lib
5. git checkout master
```

If you modify either the component schematic or footprint libraries you must push those changes to the SFUSatClub/KiCad-Lib repository.